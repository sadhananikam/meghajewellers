<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\SendMail;
use App\Mail\SendContactusMail;
use App\Mail\SendPartnerMail;
use App\Mail\SendSellerLocationEnquiryAdminMail;
use App\Newsletter;
use App\Banners;
use App\Lookbook;
use App\Products;
use App\Contact;
use App\Partners;
use App\SellerLocationEnquiry;
use Illuminate\Support\Facades\DB;

class Home extends Controller
{
    //newsletter - send email
    public function newsletter(Request $request){
        $errors = array();
        $data = array();
    
    if(empty($request->input('newsletteremail')))
        $errors['newsletteremail'] = 'Email is required';
    
        if ( ! empty($errors)) {           
		  $data['success'] = false;
		  $data['errors']  = $errors;	
        } else {
            
            $email = $request->input('newsletteremail');
            
//            $data['success'] = true;
//            $data['successMessage'] = 'Thank you.We will get back to you.';
            
            //Mail::to($email)->send(new SendMail) or die('Got Error');
            
//            Mail::send('email.newsletter',function($message){
//               $message->to($email)->subject('Test Mail'); 
//            });
            
            //\Mail::to($email)->send(new SendMail);
            
            /*
            if(\Mail::to($email)->send(new SendMail)){
                $data['success'] = true;
                $data['successMessage'] = 'Thank you.We will get back to you.';
            }else{
                $errors['errorMessage'] = 'Error';
                $data['success'] = false;
                $data['errors'] = $errors;
            }
            */
            
            $newsletter = new Newsletter;
            $newsletter->email = $email;
            $newsletter->is_active = 1;            
            
            try {
                \Mail::to($email)->send(new SendMail($email));
                
                $newsletter->save();
                
                $data['success'] = true;
                $data['successMessage'] = 'Thank you.We will get back to you.';
            }
            catch (\Exception $e) {
                $errors['errorMessage'] = $e->getMessage();
                $data['success'] = false;
                $data['errors'] = $errors;
            }
            
            
        }  
        
        return json_encode($data);
    }
    
    public function getbanners(){
        $p = $_REQUEST['p'];
        $m = $_REQUEST['m'];
        $data = array();
        
        $banners = DB::table('banners')
                    ->where('page',$p)
                    ->where('is_active',1)
                    ->where('is_Mobile',$m)
                    ->orderBy('sequence','asc')
                    ->get();
                
        return json_encode($banners);
    }
    
    public function getlookbook($id = null){
        if($id == null){
            //return Lookbook::orderBy('id','desc')->get();
            $lookbook = DB::table('lookbook')
                    ->select('id','title','urlslug','lb_short_description','lb_long_description','category','banner_img','similar_1','similar_2','similar_3','similar_4','look_1','look_2','look_3','created_at')
                    ->orderBy('id','desc')
                    ->get();
            return json_encode($lookbook);
        }else{
            return $this->show($id);
        }
    }
    
    public function getfeaturedproducts(){
        
        $featuredproducts = DB::table('products')
                    ->where('isFeatured',1)
                    ->groupBy('VC_SKU')
                    ->limit(12)
                    ->get();
                
        if($featuredproducts != null){
            return json_encode($featuredproducts);
        }
        else{
            return "invalid";
        }     
    }
    
    public function getsellerlocation()
    {
        $seller = DB::table('seller_location')
                    ->orderBy('name','asc')
                    ->get();
        return json_encode($seller);
    }
    
    public function getSellerDetailBySearch(){
        $searchtext = $_REQUEST['searchtext'];
        
        $seller = DB::table('seller_location')
                    ->where('name','like','%'.$searchtext.'%')
                    ->orWhere('address','like','%'.$searchtext.'%')
                    ->orWhere('pincode','like','%'.$searchtext.'%')
                    ->orderBy('name','asc')
                    ->get();
                
        return json_encode($seller);
    }
    
    public function saveSellerLocationEnquiry(Request $request)
    {
        $errors = array();
        $data = array();
    
        if(empty($request->input('pname')))
            $errors['name'] = 'Name is required';

        if(empty($request->input('pemail')))
            $errors['email'] = 'Email is required';

        if(empty($request->input('pphone')))
            $errors['phone'] = 'Phone is required';
        
        if(empty($request->input('plocation')))
            $errors['plocation'] = "City/Location is required";
        
        if ( ! empty($errors)) {           
		  $data['success'] = false;
		  $data['errors']  = $errors;	
        } else {
            
            $partner = new SellerLocationEnquiry;
        
            $partner->name = $request->input('pname');
            $partner->email = $request->input('pemail');
            $partner->phone = $request->input('pphone');
            $partner->location = $request->input('plocation');
            
            $data = array(
                'name' => $request->input('pname'),
                'email' => $request->input('pemail'),
                'phone' => $request->input('pphone'),
                'location' => $request->input('plocation'),
            );
            
            try {
                $partner->save();
                
                /*$emailto1 = 'sadhana@vivocarat.com';
                \Mail::to($emailto1)->send(new SendSellerLocationEnquiryAdminMail($data));*/
                $emailto1 = 'a@vivocarat.com';
                $emailto2 = 'r@vivocarat.com';
                \Mail::to($emailto1)->send(new SendSellerLocationEnquiryAdminMail($data));
                \Mail::to($emailto2)->send(new SendSellerLocationEnquiryAdminMail($data));
                
                $data['success'] = true;
                $data['successMessage'] = 'Thank you.We will get back to you.';
            }
            catch (\Exception $e) {
                $errors['errorMessage'] = $e->getMessage();
                $data['success'] = false;
                $data['errors'] = $errors;
            }
            
        }  
        
        return json_encode($data);
    }
    
    
    public function saveContactusform(Request $request)
    {
        $errors = array();
        $data = array();
    
    if(empty($request->input('cname')))
        $errors['name'] = 'Name is required';

    if(empty($request->input('cemail')))
        $errors['email'] = 'Email is required';

    if(empty($request->input('cphone')))
        $errors['phone'] = 'Phone is required';
        
        if ( ! empty($errors)) {           
		  $data['success'] = false;
		  $data['errors']  = $errors;	
        } else {
            
            $contact = new Contact;
        
            $contact->name = $request->input('cname');
            $contact->email = $request->input('cemail');
            $contact->mobile = $request->input('cphone');
            $contact->message = $request->input('cmessage');
            
            $data = array(
                'name' => $request->input('cname'),
                'email' => $request->input('cemail'),
                'phone' => $request->input('cphone'),
                'messagedata' => $request->input('cmessage')
            );
            
            try {
                $contact->save();
                
                $emailto1 = 'a@vivocarat.com';
                $emailto2 = 'r@vivocarat.com';
                \Mail::to($emailto1)->send(new SendContactusMail($data));
                \Mail::to($emailto2)->send(new SendContactusMail($data));
                
                $data['success'] = true;
                $data['successMessage'] = 'Thank you.We will get back to you.';
            }
            catch (\Exception $e) {
                $errors['errorMessage'] = $e->getMessage();
                $data['success'] = false;
                $data['errors'] = $errors;
            }
            
        }  
        
        return json_encode($data);
    }
    
    public function savePartnerform(Request $request)
    {
        $errors = array();
        $data = array();
    
    if(empty($request->input('pname')))
        $errors['name'] = 'Name is required';

    if(empty($request->input('pemail')))
        $errors['email'] = 'Email is required';

    if(empty($request->input('pphone')))
        $errors['phone'] = 'Phone is required';
        
        if ( ! empty($errors)) {           
		  $data['success'] = false;
		  $data['errors']  = $errors;	
        } else {
            
            $partner = new Partners;
        
            $partner->name = $request->input('pname');
            $partner->email = $request->input('pemail');
            $partner->phone = $request->input('pphone');
            $partner->brand = $request->input('pbrand');
            $partner->address = $request->input('paddress');
            
            $data = array(
                'name' => $request->input('pname'),
                'email' => $request->input('pemail'),
                'phone' => $request->input('pphone'),
                'brand' => $request->input('pbrand'),
                'address' => $request->input('paddress')
            );
            
            try {
                $partner->save();
                
                $emailto1 = 'a@vivocarat.com';
                $emailto2 = 'r@vivocarat.com';
                \Mail::to($emailto1)->send(new SendPartnerMail($data));
                \Mail::to($emailto2)->send(new SendPartnerMail($data));
                
                $data['success'] = true;
                $data['successMessage'] = 'Thank you.We will get back to you.';
            }
            catch (\Exception $e) {
                $errors['errorMessage'] = $e->getMessage();
                $data['success'] = false;
                $data['errors'] = $errors;
            }
            
        }  
        
        return json_encode($data);
    }
    
    public function getLookbookDetail(){
        
        $id = $_REQUEST['id'];
        //$sql2="SELECT * from lookbook where id=".$_GET['id'];
        $detail = DB::table('lookbook')
                    ->where('id',$id)
                    ->get();
        
        return json_encode($detail);
    }
    
    public function getLookbookDetailByUrlslug(){
        
        $urlslug = $_REQUEST['urlslug'];
        $detail = DB::table('lookbook')
                    ->where('urlslug',$urlslug)
                    ->get();
        
        return json_encode($detail);
    }
    
    public function getLookbookList(){
        //$sql2="SELECT * from lookbook order by id DESC";
        $lookbooklist = DB::table('lookbook')
                    ->orderBy('id','desc')
                    ->get();
        return json_encode($lookbooklist);
    }
    
    public function getRelatedBlog(){
        $id1= $_REQUEST['id1'];
        $id2 = $_REQUEST['id2'];
        $id3 = $_REQUEST['id3'];

        //$sql2="SELECT * from lookbook  where id=".$id1." or id=".$id2." or id=".$id3 ;
        
//        $relatedblog = DB::table('lookbook')
//                    ->where('id',$id1)
//                    ->orWhere('id',$id2)
//                    ->orWhere('id',$id3)
//                    ->get();
        
//        $relatedblog = DB::table('lookbook')
//                    ->whereIn('id', [$id1, $id2, $id3])
//                    ->get();
        
        $relatedblog = DB::table('lookbook')
                    ->where('id',$id1)
                    ->orWhere('id',$id2)
                    ->orWhere('id',$id3)
                    ->get();
        
        return json_encode($relatedblog);
    }
    
    public function getSimilarProducts(){
        
        $s1= $_REQUEST['s1'];
        $s2 = $_REQUEST['s2'];
        $s3 = $_REQUEST['s3'];
        $s4 = $_REQUEST['s4'];

        //$sql2="SELECT * from products  where id=".$s1." or id=".$s2." or id=".$s3." or id=".$s4 ;
        
//        $similarproducts = DB::table('products')
//                    ->where('id',$s1)
//                    ->orWhere('id',$s2)
//                    ->orWhere('id',$s3)
//                    ->orWhere('id',$s4)
//                    ->get();
        
        $similarproducts = DB::table('products')
                    ->whereIn('id', [$s1, $s2, $s3, $s4])
                    ->get();
        
        return json_encode($similarproducts);
    }
    
    public function sendSMS(Request $request)
    {
        //Your authentication key
        $authKey = "173057AunX4NLcYuf59ad5e75";

        //Multiple mobiles numbers separated by comma
        $mobileNumber = $_REQUEST['mobile'];

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "VIVOCT";

        //Your message to send, Add URL encoding here.
        $message = urlencode($_REQUEST['message']);

        //Define route 
        $route = 4;

        //Prepare you post parameters
        $postData = array(
            'sender' => $senderId,
            'message' => $message,
            'mobiles' => $mobileNumber,
            'authkey' => $authKey,
            'campaign' => 'viaSOCKET',
            'unicode' => 0,
            'flash' => 0,
            'country' => 91,
            'route' => $route
        );

        //API URL
        $url="http://api.msg91.com/api/sendhttp.php";

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


        //get response
        $output = curl_exec($ch);

        //Print error if any
        if(curl_errno($ch))
        {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);

        echo $output;
    }
}