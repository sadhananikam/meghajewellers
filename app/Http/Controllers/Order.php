<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Products;
use App\CustomersAuth;
use App\Orders;
use App\OrdersTemp;

class Order extends Controller
{
    public function getOrderHistory(){
        
        $id = $_REQUEST['uid'];

        //$sql2="SELECT * from orders_temp where uid='".$id."' order by id desc";
        
        try{

            $order = DB::table('orders_temp')
                ->where('uid',$id)
                //->where('status','CONFIRMED')
                ->where('status','<>','NOT_CONFIRMED')
                ->orderBy('id','desc')
                ->get();

            return json_encode($order);  
        }
        catch(\Exception $e){
            return $e;
        }
    }
    
    public function cancelOrder(){
        
        $oid = $_REQUEST['oid'];

        //$sql2="UPDATE orders_temp set status='CANCELLED' where id=".$oid;
        try{
            DB::table('orders_temp')
                ->where('id', $oid)
                ->update(['status' => 'CANCELLED','updated_at' => date("Y-m-d H:i:s")]);
            
            return 'success';

        }
        catch(\Exception $e){
            return $e;
        }
    }
    
    public function removeProductFromOrder(){
        $o=json_decode($_REQUEST['order']);

        $oid= $o->oid;

        $items = $o->items;

        //$sql3="UPDATE orders_temp set items='".json_encode($items)."' where id=".$oid;

        try{
            DB::table('orders_temp')
                ->where('id', $oid)
                ->update(['items' => json_encode($items),'updated_at' => date("Y-m-d H:i:s")]);
            
            return 'success';

        }
        catch(\Exception $e){
            return $e;
        }
    }
    
    public function returnOrder(){
        
        $id = $_REQUEST['oid'];
        $reason = $_REQUEST['reason'];

        //$sql2="UPDATE orders_temp set status='RETURNED_REQUESTED', return_reason='".$reason."' where id=".$id;
        try{
            DB::table('orders_temp')
                ->where('id', $id)
                ->update(['status' => 'RETURNED_REQUESTED','return_reason' => $reason,'updated_at' => date("Y-m-d H:i:s")]);
            
            return 'success';

        }
        catch(\Exception $e){
            return $e;
        }
    }
    
    public function changeItemOrder(){
        
        try{

            $order = DB::table('orders_temp')
                ->select('id','items')
                ->orderBy('id','desc')
                ->get();
            
            foreach($order as $orders)
            {
                $item = $orders->items;
                $o = json_decode($item,true);
//                return count($o);
                for($i=0;$i<count($o);$i++)
                {
                    return json_encode($o[$i]['item']['id']);
                }
//                return json_encode($o[0]['item']['id']);
//                return $item;
            }

//            return json_encode($order);  
        }
        catch(\Exception $e){
            return $e;
        }
    }
}