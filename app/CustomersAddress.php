<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomersAddress extends Model
{
    protected $table = 'customers_address';
    protected $fillable = array('uid','name','phone','address','pincode',,'city','state','country');
}
