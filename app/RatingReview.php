<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RatingReview extends Model
{
    protected $table = 'rating_review';
    protected $fillable = array('id','pid','uid','name','review','title','score');
}
