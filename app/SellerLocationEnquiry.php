<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellerLocationEnquiry extends Model
{
    protected $table = 'seller_location_enquiry';
    protected $fillable = array('id','name','location','email','phone');
}
