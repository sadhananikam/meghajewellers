var app = angular.module("vivoHome", ['vivoCommon']);

app.controller('authCtrl', function ($scope, $rootScope, $stateParams, $location, $http, Data, $window, API_URL) {
    
    /*if(getUrlParameter('utm_campaign') != null && getUrlParameter('utm_source') != null && getUrlParameter('utm_medium') != null)
    {
        var loc = $window.location.href;
        var utm_campaign = getUrlParameter('utm_campaign');
        var utm_source = getUrlParameter('utm_source');
        var utm_medium = getUrlParameter('utm_medium');
        ga('set', 'location', loc);
        ga('set', 'campaignName', utm_campaign);
        ga('set', 'campaignSource', utm_source);
        ga('set', 'campaignMedium', utm_medium);
        ga('send', 'pageview');
    }*/
    
    $scope.buyNow = function (s) {
        $rootScope.addToCart(s);
        window.location.href = "/checkout";
    }

    $("#scroller").simplyScroll();
    $scope.coupon = {
        email: null,
        password: null
    }

    $scope.goTo = function (path) {
        $('#categoryEarringsModal').modal('hide');
        $('#categoryBraceletsModal').modal('hide');
        $('#categoryPendantsModal').modal('hide');
        $('#categoryRingsModal').modal('hide');
        $('#categoryNecklacesModal').modal('hide');


        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
        window.location.href = path;
    }
    $scope.signUpCoupon = function () {
        if ($scope.coupon.email == null || $scope.coupon.email == "" || $scope.coupon.password == null || $scope.coupon.password == "") {

            $('#verifydetails').modal('show');
            $timeout(function () {
                $('#verifydetails').modal('hide');

            }, 2000);


        } else {

            $rootScope.couponSignUp($scope.coupon);
            if ($stateParams.from == 'c') {
                window.location.href = "/checkout";
            }
            $window.location.reload();
        }

    }
    $scope.showFirstModal = function () {
        if (!$rootScope.authenticated) {
            if (!$rootScope.isCouponModalShown) {
                $rootScope.isCouponModalShown = false;
            }
            if (!$rootScope.isCouponModalShown) {
                $('#registerCouponModal').modal('show');


                $rootScope.isCouponModalShown = true;
            }
        }
    }
    
    $rootScope.$on('loadHomeEvent', function () {

    });
    
    //lookbook article
     $scope.getTitle = function (id) {
        $scope.id = getUrlParameter('id');
         
         $http({
            method: 'GET',
            url : API_URL + 'getLookbookList'
        }).then(function successCallback(response){
            $scope.title = response.data;
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    /*$http({
        method: 'GET',
        url : API_URL + 'getVivoHeader',
        params : {p:'home',m:1}
    }).then(function successCallback(response){
        console.log(response.data);
        $scope.banners = response.data;
    },function errorCallback(response){
        console.log(response.data);
    });*/
     
     $scope.banners = [{"alt":"MEGHA JEWELLERS","href":"/brands/Megha%20Jewellers","is_link":"1","target":"_self","img_url":"https://www.vivocarat.com/images/mobile/home/carousel banner/Sarvada-VivoCarat-mob-n.jpg"}];
    
    $http({
        method: 'GET',
        url : API_URL + 'getfeaturedproducts'
    }).then(function successCallback(response){
        $scope.list = response.data;
        if (response.data == "invalid") {
        
            $('#invalidUrl').modal('show');
            $timeout(function () {
                $('#invalidUrl').modal('hide');

            }, 2000);

        } else {
            var result = response.data;

        }
    },function errorCallback(response){
        console.log(response.data);
    });
    
    //to format date in dd mmmm
    $scope.formatDate = function(date){
          var dateOut = new Date(date);
          return dateOut;
    };
    
    $scope.getTitle();

});