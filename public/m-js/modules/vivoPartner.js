var partnerapp = angular.module("vivoPartner", ['vivoCommon']);

partnerapp.controller("partnerCtrl", ["$scope", "$http", "API_URL", function ($scope, $http, API_URL) {
    
    $scope.resetP = function () {
        $scope.p = {
            pname: null,
            pbrand: null,
            pphone: null,
            pemail: null,
            pmessage: null
        }

    }
    $scope.resetP();

    $scope.addPartner = function () {
        
        waitingDialog.show();
        var url = API_URL + 'partner';
        
        $http({
            method: 'POST',
            url: url, 
            data: $.param($scope.p),
            headers: { 'Content-type':'application/x-www-form-urlencoded' }           
        })
        .then(function successCallback(response){

            if (response.data.success == false) {
                // if not successful, bind errors to error variables
                var msg = '';
                if(response.data.errors.name)
                {
                    msg = msg + response.data.errors.name + "\n";
                }
                
                if(response.data.errors.email)
                {
                    msg = msg + response.data.errors.email + "\n";
                }
                
                if(response.data.errors.phone)
                {
                    msg = msg + response.data.errors.phone + "\n";
                }
                
              if(response.data.errors.errorMessage)
                {
                    msg = msg + response.data.errors.errorMessage;
                }
                
                BootstrapDialog.show({
                    type:BootstrapDialog.TYPE_DANGER,
                    title:'Error',
                    message:msg
                });
                
                waitingDialog.hide();
            } 
            else {
                // if successful, bind success message to message                
               waitingDialog.hide();
                //            alert("Thanks for contacting us. We will get in touch with you shortly.");
                $("#addpartner").modal('show');

                $timeout(function () {
                    $("#addpartner").modal('hide');
                }, 2000);

                $scope.resetP();
            }
        },function errorCallback(response){
            console.log(response.data);
            //alert('An error has occured. Please check the log for details');
            BootstrapDialog.show({
                type:BootstrapDialog.TYPE_DANGER,
                title:'Error',
                message:'An error has occured. Please check the log for details'
            });
            waitingDialog.hide();
        });
    }
    }]);