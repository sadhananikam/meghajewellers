<html>
 <head>
  <style>
        @font-face {
            font-family: 'leela';
            src: url('https://www.vivocarat.com/fonts/LEELAWADEE.TTF') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
        
        /* For Outlook email client compatibility*/
        
        @media screen {
            .webfont {
                font-family: 'Leelawadee', Roboto, Arial, sans-serif !important;
            }
        }
        /*End of compatibility css*/
        
        body {
            font-family: 'Leelawadee', Roboto, sans-serif;
            font-size: 15px;
            margin-left: auto;
            margin-right: auto;
            background-color: #f3f3f3;

        }
</style>

    <!--Web font-->
    <link href='https://www.vivocarat.com/fonts/LEELAWADEE.TTF' rel='stylesheet' type='text/css'>
    
    
</head>

<body align='center' style="width:600px;text-align:center;font-family: 'Leelawadee', Roboto, sans-serif;font-size: 15px;margin-left: auto;margin-right: auto;background-color: #f3f3f3;">

<table align='center' style='padding-top: 10px;padding-bottom: 10px;'>
 <tr>
  <td align='center'>
   <img src='https://www.vivocarat.com/images/emailers/rounded-logo.png' alt='logo'>
  </td>
 </tr>
</table>    

<div style='text-align:left;background-color:white;padding-left:10px;padding-right:10px;padding-top: 20px;padding-bottom: 20px;width: 600px;'>

    <p>Hi {{ $name }},</p>
    <p>Please click on the link below to reset your password</p>
    <p><a href="{{ $resetUrl }}">{{ $resetUrl }}</a></p>
                
</div>

<table style='background-color:#E62739;width: 600px;'>
        <tr>
            <td align='center' style='font-size:15px;color:white;text-align: center;padding:7px;color:white;'>CALL US: +91 9167 645 314</td>
            <td align='center' style='width:15%'></td>
            <td align='center' style='font-size:15px;color:white;text-align: center;padding:7px;color:white;'>EMAIL US: <a href='mailto:hello@vivocarat.com' style='text-decoration:none;color:white;'>hello@vivocarat.com</a></td>
        </tr>
</table>


 <div style='height: 7%;padding-top: 20px;padding-bottom: 20px;text-align: center;border: 1px solid #d4d4d4;font-weight: 500;background-color: white;width: 600px;'>
        <p style='font-size:12px; margin:0px;'>Privacy Policy | Terms & Conditions</p>
        <p style='font-size:12px; margin:0px;'>&copy;&nbsp;2016 VivoCarat Retail Pvt. Ltd.All Rights Reserved.</p>
  </div>
  
  
</body>

</html>