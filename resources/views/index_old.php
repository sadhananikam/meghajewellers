<!DOCTYPE html>
<html lang="en" data-ng-app="vivo">

<head>

    <meta http-equiv="Content-Language" content="en" />

    <meta name="keywords" content="" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vivocarat">
    <meta name="author" content="Vivo">
    <link rel="icon" href="images/header/logos/favicon.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="http://www.vivocarat.com" />
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />

    <link rel="canonical" href="http://www.vivocarat.com/" />

    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    <link rel="stylesheet" href="css/kendo.common-material.min.css" />
    <link rel="stylesheet" href="css/kendo.material.min.css" />


    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1026045347487468&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->

    <link rel="stylesheet" href="css/arthref.css">
    <!--    <script src="//kendo.cdn.telerik.com/2016.1.112/js/angular.min.js"></script>-->

    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/form.css" rel="stylesheet" type="text/css" media="all" />

    <script src="js/jquery.js"></script>
    <!--start slider -->
    <link rel="stylesheet" href="css/fwslider.css">
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/css3-mediaqueries.js"></script>

    <!--end slider -->
    <!-- start menu -->
    <link href="css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/magnific-popup.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/etalage.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/angular.rangeSlider.css" rel="stylesheet" type="text/css" media="all" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="css/daterangepicker.css" />
    <script type="text/javascript" src="js/megamenu.js"></script>


    <script src="js/slides.min.jquery.js">
    </script>
    <script type="text/javascript" src="js/jquery.jscrollpane.min.js"></script>
    <script type="text/javascript" src="js/jquery.easydropdown.js"></script>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/bootstrap.min.js"></script>

    <script src="js/custom.js"></script>


    <script src="js/socialShare.js"></script>
    <!-- <script src="http://maps.googleapis.com/maps/api/js?key=&sensor=false&extension=.js"></script>-->
    <script src="js/angular.min.js"></script>
    <script src="js/angular-ui-router.min.js"></script>
    <script src="js/angular-animate.min.js"></script>
    <script src="js/angular-sanitize.js"></script>
    <script src="js/satellizer.min.js"></script>
    <script src="js/angular.rangeSlider.js"></script>
    <script src="js/select.js"></script>
    <script src="js/toaster.js"></script>
    <script src="js/kendo.all.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="js/updateMeta.js"></script>
    <script src="js/update-meta.directive.js"></script>
    <script src="js/update-title.directive.js"></script>
    <script src="js/taggedInfiniteScroll.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <!--  <script src="js/bootstrap.min.js"></script>-->

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/angular-google-plus.min.js"></script>

    <script src="js/jquery.etalage.min.js"></script>
    <script src="js/jquery.magnific-popup.js"></script>

    <script src="app/app.js"></script>
    <script src="app/data.js"></script>
    <script src="app/directives.js"></script>
    <script src="app/authCtrl.js"></script>
    <script src="device-router.js"></script>


    <script type="text/javascript" src="js/moment.min.js"></script>
    <script type="text/javascript" src="js/daterangepicker.js"></script>




    <!--Scroller script-->
    <script type="text/javascript" src="js/jquery.simplyscroll.js"></script>
    <!--    <link rel="stylesheet" href="js/jquery.simplyscroll.css" media="all" type="text/css">-->






</head>

<body style="overflow-x: hidden;">

    <style>

        
/*
        .slider {
            width: 100%;
            overflow: hidden;
            position: relative;
            margin: 0;
            border: 1px solid #eee;
            padding: 10px;
        }
        
        .edge {
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            position: absolute;
            height: 100%;
            display: block;
        }
        
        .edge:before {
            content: '';
            position: absolute;
            left: 0;
            background: -webkit-linear-gradient(left, white 10%, rgba(0, 0, 0, 0) 100%);
            width: 25%;
            height: 100%;
        }
        
        .edge:after {
            content: '';
            position: absolute;
            right: 0;
            background: -webkit-linear-gradient(right, white 10%, rgba(0, 0, 0, 0) 100%);
            width: 25%;
            height: 100%;
        }
        
        .testi {
            height: 220px
        }
        
        .home-panels {
            width: 100%;
            height: 100%;
        }
        
        .home-panels:hover {
            cursor: pointer;
        }
        
        .home-panels-text {
            border: 1px solid #888888;
            border-top: none !important;
            box-shadow: 2px 2px 10px 1px #888888;
            padding: 20px;
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
            text-align: center;
            font-size: 20px;
            color: #E62739 !important;
        }
        
        .quote {
            color: rgba(0, 0, 0, .1);
            text-align: center;
            margin-bottom: 30px;
        }
        
        
       
        //    Carousel Fade Transition   
        
        
        #fade-quote-carousel.carousel {
            padding-bottom: 60px;
        }
        
        #fade-quote-carousel.carousel .carousel-inner .item {
            opacity: 0;
            -webkit-transition-property: opacity;
            -ms-transition-property: opacity;
            transition-property: opacity;
        }
        
        #fade-quote-carousel.carousel .carousel-inner .active {
            opacity: 1;
            -webkit-transition-property: opacity;
            -ms-transition-property: opacity;
            transition-property: opacity;
        }
        
        #fade-quote-carousel.carousel .carousel-indicators {
            bottom: 10px;
        }
        
        #fade-quote-carousel.carousel .carousel-indicators > li {
            background-color: #e2e2e2 !important;
            border: none;
        }
        
        #fade-quote-carousel.carousel .carousel-indicators > li.active {
            background-color: #E62739 !important;
            border: none;
        }
        
        #fade-quote-carousel blockquote {
            text-align: center;
            border: none;
        }
        
        #fade-quote-carousel .profile-circle {
            width: 100px;
            height: 100px;
            margin: 0 auto;
            border-radius: 100px;
        }
        
        //END of transition
        
        .inside-shadow {
            -webkit-box-shadow: inset 0px 0px 110px -14px rgba(210, 210, 210, 0.59);
            -moz-box-shadow: inset 0px 0px 110px -14px rgba(210, 210, 210, 0.59);
            box-shadow: inset 0px 0px 110px -14px rgba(210, 210, 210, 0.59);
        }
        
        ul.slideUl {
            overflow: hidden;
            width: 1000%;
            margin: 0;
        }
        
        li.slideItem {
            list-style: none;
            display: inline-block;
            padding: 0 50px;
        } 
*/

        
        /*Image scroller css */
        /* Container DIV - automatically generated */
        
        .simply-scroll-container1 {
            position: relative;
        }
        /* Clip DIV - automatically generated */
        
        .simply-scroll-clip {
            position: relative;
            overflow: hidden;
        }
        /* UL/OL/DIV - the element that simplyScroll is inited on
Class name automatically added to element */
        
        .simply-scroll-list {
            overflow: hidden;
            margin: 0;
            padding: 0;
            list-style: none;
        }
        
        .simply-scroll-list img {
            padding: 0px 10px 0px 10px;
            margin: 0;
            list-style: none;
        }
        
        .simply-scroll-list img {
            border: none;
            display: block;
        }
        /* Custom class modifications - adds to / overrides above

.simply-scroll is default base class */
        /* Container DIV */
        
        .simply-scroll {
            width: 1140px;
            /*Below attributes added to contain the scroller into container*/
            margin-left: -102px;
            padding-left: 89px;
        }
        /* Clip DIV */
        
        .simply-scroll .simply-scroll-clip {
            width: 1123px;
            /*        height: 200px;*/
            /*Margin set to make it appear within the border*/
            margin-left: -77px;
        }
        /* Explicitly set height/width of each list item */
        
        .simply-scroll .simply-scroll-list img {
            float: left;
            /* Horizontal scroll only */
            width: 215px;
            /*        height: 75px;*/
            max-width: 100%;
            vertical-align: baseline;
        }
        /*END of image scroller css*/
        /*Media queries for scroller,category and others*/
        
        @media (min-width: 2560px) {
            .infyscroll {
                margin-top: 417px !important;
            }
        }
        
        @media (min-width: 1920px) {
            .infyscroll {
                margin-top: 189px !important;
            }
        }
        
        @media (min-width: 1366px) {
            .infyscroll {
                margin-top: -45px !important;
            }
        }
        
        .title-background{
           background-image:url('images/lookbook/lb_image1.png');
           background-repeat: no-repeat;
           background-position: 100%;
           margin:0px;
           background-color: #ffffff;
           padding-top: 14px;
           padding-bottom: 13px;
           padding-left: 5px;
           font-family: 'leela';
           font-size: 15px;
           font-weight: bold;
        }
        
        .scroller-parent-container{
            position:relative;
            padding: 30px 0px 30px 90px;
            border: 1px solid #d1d1d1;
            z-index: 1;
            background-color: white;
            margin-left: -1px !important;
            margin-right: -24px !important;
            width: 1126px !important;
            margin-top:-80px;
        }
</style>

<vivo-header></vivo-header>

<div data-ng-controller='authCtrl'>


        <!--Full screen carousel-->
<!--
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="position:relative;z-index:1">
            
  <ol class="carousel-indicators" style="margin-bottom: 0;">
             
   <li data-target="#carousel-example-generic" data-slide-to="0" class='active'></li>
                
   <li data-ng-repeat="b in banners" data-target="#carousel-example-generic" data-slide-to="$index+1"></li>

  </ol>

           
  <div class="carousel-inner" role="listbox">
                
   <div class="item active row">
    <a href="p-search.html?text=engagement-women">
     <div class="col-md-6 no-padding-left-right">
      <img src="images/banners/left engagement.jpg" class="width-100-percent">   
     </div>
    </a>
                 
    <a href="p-search.html?text=engagement-men">
     <div class="col-md-6 no-padding-left-right">
      <img src="images/banners/right engagment.jpg" class="width-100-percent">  
     </div>  
    </a>

   </div>


   <div data-ng-repeat="b in banners" class="item">
    <a data-ng-if="b.is_link==1" href="{{b.href}}" target="{{b.target}}">
     <img class="ban-img img-responsive" ng-src="{{b.img_url}}"/>
    </a>

    <img data-ng-if="b.is_link==0" class="ban-img img-responsive" src="{{b.img_url}}" alt="" style="height:calc(100vh - 173px) !important;;" />
   </div>

 </div>
    
            
 <div class="left carousel-control banner-arrow-structure" data-target="#carousel-example-generic" role="button" data-slide="prev">
  <img class="center-block banner-arrow-image-position" src="images/home/icons/scroll_left_hover.png">
 </div>

 <div class="right carousel-control banner-arrow-structure" data-target="#carousel-example-generic" role="button" data-slide="next">
  <img class="center-block banner-arrow-image-position" src="images/home/icons/scroll_right_hover.png">
 </div>
</div>
-->
    
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="position:relative;z-index:1">
    <!-- Indicators -->
    <ol class="carousel-indicators" style="margin-bottom: 0;">

        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        <li data-target="#carousel-example-generic" data-slide-to="3"></li>
        <li data-target="#carousel-example-generic" data-slide-to="4"></li>




    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <!--Dynamic banners commentd to show single image for designer's need-->
        <div data-ng-repeat="b in banners" class="item" data-ng-class="$index==0?'active':''">
            <a data-ng-if="b.is_link==1" href="{{b.href}}" target="{{b.target}}">
                <img class="ban-img img-responsive" ng-src="{{b.img_url}}"/>
            </a>

            <img data-ng-if="b.is_link==0" class="ban-img img-responsive" src="{{b.img_url}}" style="height:calc(100vh - 173px) !important;;"/>
        </div>


    </div>
    <!-- Controls -->
    <div class="left carousel-control banner-arrow-structure" data-target="#carousel-example-generic" role="button" data-slide="prev">
        <img class="center-block banner-arrow-image-position" src="images/home/icons/scroll_left_hover.png">
    </div>

    <div class="right carousel-control banner-arrow-structure" data-target="#carousel-example-generic" role="button" data-slide="next">
        <img class="center-block banner-arrow-image-position" src="images/home/icons/scroll_right_hover.png">
    </div>
</div>

<div class="container">
          
 <div class="modal fade" id="registerCouponModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" style="width: 900px;" role="document">
   <div class="modal-content" style="border-radius: 0px;">

    <div class="modal-body" style="padding: 0px;">

     <div class="row">
      <div class="col-lg-7" style="padding: 0px;">
       <img src="images/home/modals/modal2.jpg" style="width: 100%;height: 500px;">
      </div>
      <div class="col-lg-5" style="padding:0px">
       <form>
        <img src="images/home/modals/modal1.jpg" style="height: 250px;width: 100%;">
        <div style="padding:30px;padding-top:60px;background-image:url('images/home/modals/modal3.jpg');height: 250px;background-size: cover;">
            
        <input type="email" data-ng-model="coupon.email" placeholder="E-mail" class="form-control">
            
        <br/>
            
        <input type="text" data-ng-model="coupon.password" placeholder="password" class="form-control">
            
        <br/>
            
        <a class="btn btn-lg btn-block btn-vivo" data-ng-click="signUpCoupon()"> 
            Register
        </a>
        </div>
       </form>
      </div>
     </div>
    </div>

  </div>
 </div>
</div>
       


<div class="container infyscroll scroller-parent-container">
 <div id="scroller" class="img-responsive" style="max-width:none;">

  <img class="first pointer" id="incocu" src="images/header/brands logos dropdown/Incocu Jewellers hover.png">
                    
  <img class="pointer" id="shankaram" src="images/header/brands logos dropdown/Shankaram Jewellers hover.png">
                    
  <img class="pointer" id="lagu" src="images/header/brands logos dropdown/Lagu Bandhu hover.png">
                    
  <img class="pointer" id="arkina" src="images/header/brands logos dropdown/Arkina-Diamonds hover.png">
                    
  <img class="pointer" id="ornomart" src="images/header/brands logos dropdown/OrnoMart hover.png">
                    
  <img class="pointer" id="kundan" src="images/header/brands logos dropdown/Kundan Jewellers hover.png">
                    
  <img class="pointer" id="mayura" src="images/header/brands logos dropdown/Mayura Jewellers hover.png">
                    
  <img class="pointer" id="megha" src="images/header/brands logos dropdown/Megha Jewellers hover.png">
     
  <img class="pointer" id="regaalia" src="images/header/brands logos dropdown/Regaalia Jewels hover.png">
     
  <img class="pointer" id="glitter" src="images/header/brands logos dropdown/Glitter Jewels hover.png">
     
<!--  <img class="pointer" id="mani" src="images/header/brands logos dropdown/Mani Jewels hover.png">-->
     
  <img class="pointer" id="pp" src="images/header/brands logos dropdown/PP-Gold hover.png">
     
  <img class="pointer" id="karatcraft" src="images/header/brands logos dropdown/KaratCraft hover.png">
     
  <img class="pointer" id="zkd" src="images/header/brands logos dropdown/ZKD-Jewels hover.png">
     
  <img class="pointer" id="iskiuski" src="images/header/brands logos dropdown/Iski-Uski hover.png">
     
  <img class="pointer" id="myrah" src="images/header/brands logos dropdown/Myrah-Silver-Works hover.png">
     
  <img class="pointer" id="charu" src="images/header/brands logos dropdown/Charu-Jewels hover.png">

 </div>
</div>

           
<div class="row padding-bottom-20px">
 <div class="col-lg-3 vivocarat-heading">
      CATEGORIES
 </div>

 <div class="col-lg-9 categoryGrad no-padding-left-right margin-top-68px">

 </div>
</div>

<div class="row">
    
 <div data-ng-init="wedding='images/home/categories/wedding collection.jpg'" data-ng-mouseout="wedding='images/home/categories/wedding collection.jpg'" data-ng-mouseover="wedding='images/home/categories/wedding collection hover.jpg'" class="col-lg-6 no-padding-right">
  <a href="p-list.html?type=gold&subtype=earrings">
   <img ng-src="{{wedding}}" alt="studs" title="studs">
  </a>
 </div>

 <div class="col-lg-6 no-padding-left-right">
     
  <div class="row">
   <div class="col-lg-12 no-padding-left">
    <a href="p-list.html?type=diamond&subtype=rings">
     <img src="images/home/categories/rings.jpg" alt="Diamond Rings" title="Diamond Rings">
    </a>
   </div>
  </div>

  <div class="row">
   <div class="col-lg-12 no-padding-left">
    <a href="p-list.html?type=diamond&subtype=pendants">
     <img src="images/home/categories/pendants.jpg" alt="Diamond Pendants" title="Diamond Pendants">
    </a>
   </div>
  </div>

 </div>
    
</div>


<div class="row">
    
 <div class="col-lg-6 no-padding-left-right">
     
  <div class="row">
   <div class="col-lg-12 no-padding-right">
    <a href="p-list.html?type=diamond&subtype=rings">
     <img src="images/home/categories/bangles.jpg" alt="Diamond Rings" title="Diamond Rings">
    </a>
   </div>
  </div>

  <div class="row">
   <div class="col-lg-12 no-padding-right">
    <a href="p-list.html?type=diamond&subtype=nosepins">
     <img src="images/home/categories/nosepins.jpg" alt="Diamond Nosepins" title="Diamond Nosepins">
    </a>
   </div>
  </div>

 </div>

 <div class="col-lg-6 no-padding-left">
  <a href="p-list.html?type=diamond&subtype=earrings">
    <img src="images/home/categories/earrings.jpg" alt="Diamond Earrings" title="Diamond Earrings">
  </a>
 </div>

</div>

                
<div class="row padding-bottom-20px">
 <div class="col-lg-4 vivocarat-heading">
      FEATURED PRODUCTS
 </div>

 <div class="col-lg-7 categoryGrad no-padding-left-right margin-top-68px">

 </div>
                        
 <div class="col-lg-1 controls pull-right featured-products-arrow-structure">

  <a data-ng-init="look1='images/home/icons/FL.png'" data-ng-mouseout="look1='images/home/icons/FL.png'" data-ng-mouseover="look1='images/home/icons/FL hover.png'" class="left carousel-control featured-products-arrow-background" data-target="#carousel-example-feat" data-slide="prev">
   <img class="featured-products-left-arrow-position" ng-src="{{look1}}">
  </a>

  <a data-ng-init="look2='images/home/icons/FR.png'" data-ng-mouseout="look2='images/home/icons/FR.png'" data-ng-mouseover="look2='images/home/icons/FR hover.png'" class="right carousel-control featured-products-arrow-background" data-target="#carousel-example-feat" data-slide="next">
    <img class="featured-products-right-arrow-position" ng-src="{{look2}}">
  </a>
 </div>
                        
</div>


                    
<div id="carousel-example-feat" class="carousel slide" data-ride="carousel" style="position:relative;z-index:1;padding-top: 50px;">

       <!-- Wrapper for slides -->
<div class="height carousel-inner">
 <div class="item active row">
     
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a href="p-product.html?id={{list[0].id}}&title={{list[0].slug}}">
      
     <div class="product_image">
      <img src="images/products-v2/{{list[0].VC_SKU}}-1.jpg"/>
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[0].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center">
       <a class="btn btn-book" data-ng-click="buyNow(list[0])"> 
                Buy Now
       </a>
      </div>
     </div>

     
    
    </a>
   </div>
   
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a href="p-product.html?id={{list[1].id}}&title={{list[1].slug}}">
      
     <div class="product_image">
      <img src="images/products-v2/{{list[1].VC_SKU}}-1.jpg"/>
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[1].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center">
       <a class="btn btn-book" data-ng-click="buyNow(list[1])"> 
                Buy Now
       </a>
      </div>
     </div>

     
    
    </a>
   </div>
   
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a href="p-product.html?id={{list[2].id}}&title={{list[2].slug}}">
      
     <div class="product_image">
      <img src="images/products-v2/{{list[2].VC_SKU}}-1.jpg"/>
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[2].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center">
       <a class="btn btn-book" data-ng-click="buyNow(list[2])"> 
                Buy Now
       </a>
      </div>
     </div>

     
    
    </a>
   </div>
     
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a href="p-product.html?id={{list[3].id}}&title={{list[3].slug}}">
      
      <img src="images/products-v2/{{list[3].VC_SKU}}-1.jpg"/>
     
     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[3].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center">
       <a class="btn btn-book" data-ng-click="buyNow(list[3])"> 
                Buy Now
       </a>
      </div>
     </div>

     
    
    </a>
   </div>
     
  </div>
     
 <div class="item row">
     
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a href="p-product.html?id={{list[4].id}}&title={{list[4].slug}}">
      
     <div class="product_image">
      <img src="images/products-v2/{{list[4].VC_SKU}}-1.jpg"/>
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[4].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center">
       <a class="btn btn-book" data-ng-click="buyNow(list[4])"> 
                Buy Now
       </a>
      </div>
     </div>

     
    
    </a>
   </div>
   
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a href="p-product.html?id={{list[5].id}}&title={{list[5].slug}}">
      
     <div class="product_image">
      <img src="images/products-v2/{{list[5].VC_SKU}}-1.jpg"/>
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[5].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center">
       <a class="btn btn-book" data-ng-click="buyNow(list[5])"> 
                Buy Now
       </a>
      </div>
     </div>

     
    
    </a>
   </div>
   
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a href="p-product.html?id={{list[6].id}}&title={{list[6].slug}}">
      
     <div class="product_image">
      <img src="images/products-v2/{{list[6].VC_SKU}}-1.jpg"/>
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[6].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center">
       <a class="btn btn-book" data-ng-click="buyNow(list[6])"> 
                Buy Now
       </a>
      </div>
     </div>

     
    
    </a>
   </div>
     
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a href="p-product.html?id={{list[7].id}}&title={{list[7].slug}}">
      
      <img src="images/products-v2/{{list[7].VC_SKU}}-1.jpg"/>
     
     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[7].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center">
       <a class="btn btn-book" data-ng-click="buyNow(list[7])"> 
                Buy Now
       </a>
      </div>
     </div>

     
    
    </a>
   </div>
     
  </div>
    
 <div class="item row">
     
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a href="p-product.html?id={{list[8].id}}&title={{list[8].slug}}">
      
     <div class="product_image">
      <img src="images/products-v2/{{list[8].VC_SKU}}-1.jpg"/>
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[8].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center">
       <a class="btn btn-book" data-ng-click="buyNow(list[8])"> 
                Buy Now
       </a>
      </div>
     </div>

     
    
    </a>
   </div>
   
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a href="p-product.html?id={{list[9].id}}&title={{list[9].slug}}">
      
     <div class="product_image">
      <img src="images/products-v2/{{list[9].VC_SKU}}-1.jpg"/>
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[9].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center">
       <a class="btn btn-book" data-ng-click="buyNow(list[9])"> 
                Buy Now
       </a>
      </div>
     </div>

     
    
    </a>
   </div>
   
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a href="p-product.html?id={{list[10].id}}&title={{list[10].slug}}">
      
     <div class="product_image">
      <img src="images/products-v2/{{list[10].VC_SKU}}-1.jpg"/>
     </div>

     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[10].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center">
       <a class="btn btn-book" data-ng-click="buyNow(list[10])"> 
                Buy Now
       </a>
      </div>
     </div>

     
    
    </a>
   </div>
     
   <div class="col-sm-3 no-padding-left-right inner_content1 clearfix">
    <a href="p-product.html?id={{list[11].id}}&title={{list[11].slug}}">
      
      <img src="images/products-v2/{{list[7].VC_SKU}}-1.jpg"/>
     
     <div class="row">
      <div class="col-sm-12 grid-price-after1">
          RS. {{list[11].price_after_discount | INR}}
      </div>
     </div>

     <div class="row margin-bottom-10px padding-top-10px">
      <div class="col-sm-12 text-center">
       <a class="btn btn-book" data-ng-click="buyNow(list[11])"> 
                Buy Now
       </a>
      </div>
     </div>

     
    
    </a>
   </div>
     
  </div>
    
</div>   
</div>       
           
    
               
                

<div class="row padding-bottom-20px">
 <div class="col-lg-3 vivocarat-heading">
      OUR CATALOGUE
 </div>

 <div class="col-lg-9 categoryGrad no-padding-left-right margin-top-68px">

 </div>
</div>

<div class="row">
 <div class="col-lg-6 no-padding-right">
  <img src="images/home/catalogue/engagement.jpg">
 </div>

 <div class="col-lg-6 no-padding-left">

  <div class="row">
   <div class="col-lg-6 no-padding-left-right">
    <a href="p-list.html?type=Gold&subtype=All">
     <img src="images/home/catalogue/gold.jpg">
    </a>
   </div>

   <div class="col-lg-6 no-padding-left-right">
    <a href="p-list.html?type=Diamond&subtype=All">
     <img src="images/home/catalogue/diamond.jpg">
    </a>
   </div>

  </div>

 </div>

</div>


<div class="row padding-bottom-20px">
 <div class="col-lg-2 vivocarat-heading">
          LOOKBOOK
 </div>

 <div class="col-lg-10 categoryGrad no-padding-left-right margin-top-68px">

 </div>
</div>

            
</div>
<!--End of container div-->


<div class="row">
 <div class="col-md-12 lookbook-structure">
             
 <div class="container">
  <div class="row">
               
   <div class="col-md-4">
    <p class="normal-text margin-none padding-bottom-30px">
     Stay tuned to stay updated! Treat us as your personal stylist and let us take you to the world of fad and fancy.
    </p>
                
    <div class="row" style="border:1px solid #d5d5d5;">
     <div class="col-md-12 no-padding-left-right">
      <a href="p-lookbookArticle.html?id={{title[0].id}}">
       <p class="title-background">
          {{title[0].title}}
       </p>
      </a>
     </div>
    </div>

    <div class="row" style="border:1px solid #d5d5d5;border-top: transparent;">
     <div class="col-md-12 no-padding-left-right">
      <img src="images/lookbook/blogs/{{title[0].banner_img}}">
     </div>
    </div>
                
    <div class="row lookbook-footer">
     <div class="col-md-8">
          {{title[0].category}}
     </div>

     <div ng-bind="formatDate(date) |  date:'dd MMMM'" class="col-md-4 text-right">
          {{title[0].created_at}}
     </div>
    </div>
   </div>   
            
   <div class="col-md-4">
                
    <div class="row" style="border:1px solid #d5d5d5;">
     <div class="col-md-12 no-padding-left-right">
      <a href="p-lookbookArticle.html?id={{title[1].id}}">
       <p class="title-background">
          {{title[1].title}}
       </p>
      </a>
     </div>
    </div>

    <div class="row" style="border:1px solid #d5d5d5;border-top: transparent;">
     <div class="col-md-12 no-padding-left-right">
      <img src="images/lookbook/blogs/{{title[1].banner_img}}">
     </div>
    </div>
                
    <div class="row lookbook-footer">
     <div class="col-md-8">
          {{title[1].category}}
     </div>

     <div ng-bind="formatDate(date) |  date:'dd MMMM'" class="col-md-4 text-right">
         {{title[1].created_at}}
     </div>
    </div>
                
    <div class="row">
     <div class="col-md-12 padding-top-30px text-center">
      <a class="btn browse-more-button" href="p-lookbook.html"> 
         Browse more styles 
      </a>
     </div>
    </div>

  </div>
               
  <div class="col-md-4">
   <p class="normal-text margin-none padding-bottom-30px">
    Our lookbook features trendy posts to enlighten you with the latest who’s who of fashion, jewellery, celebrity style and much more.
   </p>
                
   <div class="row" style="border:1px solid #d5d5d5;">
    <div class="col-md-12 no-padding-left-right">
     <a href="p-lookbookArticle.html?id={{title[2].id}}">
      <p class="title-background">
         {{title[2].title}}
      </p>
     </a>
    </div>
   </div>

   <div class="row" style="border:1px solid #d5d5d5;border-top: transparent;">
    <div class="col-md-12 no-padding-left-right">
     <img src="images/lookbook/blogs/{{title[2].banner_img}}">
    </div>
   </div>
                
   <div class="row lookbook-footer">
    <div class="col-md-8">
         {{title[2].category}}
    </div>

    <div ng-bind="formatDate(date) |  date:'dd MMMM'" class="col-md-4 text-right">
         {{title[2].created_at}}
    </div>
   </div>
  </div>   
               
 </div>
</div>
<!--END of container div-->
             
 </div>
</div>

<div class="container">
            
<div class="row padding-bottom-20px">
 <div class="col-lg-3 vivocarat-heading">
      TESTIMONIAL
 </div>

 <div class="col-lg-9 categoryGrad no-padding-left-right margin-top-68px">

 </div>
</div>
            
            
<div class="carousel slide" id="testimonial">
            
<div class="carousel-inner" role="listbox"> 
             
 <div class="item row active testimonial-side-space">
             
  <div class="col-md-4">
              
   <div class="row">
    <div class="col-md-12 text-center testimonial-structure-1">
     <img class="img-circle testimonial-image-structure" src="images/home/testimonial/benson.jpg">
             
     <p class="testimonial-text">
                 
      <img src="images/home/icons/opening quote.png" class="opening-quote-position">
                 
      Beautiful collection of jewellery. The delivery packaging is the best amongst all ecomm sites. One happy customer.
                 
     </p> 
             
     <img src="images/home/icons/closing quote.png" class="closing-quote-position">
                
    </div>
   </div>
              
  </div>
             
  <div class="col-md-4">
              
   <div class="row">
    <div class="col-md-12 text-center testimonial-structure-2">
     <img class="img-circle testimonial-image-structure" src="images/home/testimonial/ajinkya.jpg">
             
     <p class="testimonial-text">
                 
     <img src="images/home/icons/opening quote.png" class="opening-quote-position">
                 
     Had to get an anniversary gift for my wife. VivoCarat delivered the necklace in 5 days. Earlier than what was promised. Strongly recommend it.
                 
     </p> 
             
     <img src="images/home/icons/closing quote.png" class="closing-quote-position"> 
                
    </div>
   </div>
              
  </div>
             
  <div class="col-md-4">
              
   <div class="row">
    <div class="col-md-12 text-center testimonial-structure-3">
     <img class="img-circle testimonial-image-structure" src="images/home/testimonial/sarika.jpg">
             
     <p class="testimonial-text">
                 
      <img src="images/home/icons/opening quote.png" class="opening-quote-position">
                 
      VivoCarat sells the best jewellery available online. Smooth ordering and express delivery. Loved it.
                 
     </p> 
             
     <img src="images/home/icons/closing quote.png" class="closing-quote-position"> 
                
    </div>
   </div>
              
   </div>
             
 </div>
          
<!--
 <div class="item row testimonial-side-space">
             
  <div class="col-md-4">
              
   <div class="row">
    <div class="col-md-12 text-center testimonial-structure-1">
     <img class="img-circle testimonial-image-structure" src="images/home/testimonial/sarika.jpg">
             
     <p class="testimonial-text">
                 
      <img src="images/home/icons/opening quote.png" class="opening-quote-position">
                 
      This is my testimonial.Read reviews also.VivoCarat delivered the necklace in 5 days.
                 
     </p> 
             
     <img src="images/home/icons/closing quote.png" class="closing-quote-position"> 
                
    </div>
   </div>
              
  </div>
             
  <div class="col-md-4">
              
   <div class="row">
    <div class="col-md-12 text-center testimonial-structure-2">
     <img class="img-circle testimonial-image-structure" src="images/home/testimonial/sarika.jpg">
             
     <p class="testimonial-text">
                 
      <img src="images/home/icons/opening quote.png" class="opening-quote-position">
                 
      This is my testimonial.Read reviews also.VivoCarat delivered the necklace in 5 days.
                 
     </p> 
             
     <img src="images/home/icons/closing quote.png" class="closing-quote-position"> 
                
    </div>
   </div>
              
  </div>
             
  <div class="col-md-4">
              
   <div class="row">
    <div class="col-md-12 text-center testimonial-structure-3">
     <img class="img-circle testimonial-image-structure" src="images/home/testimonial/sarika.jpg">
             
     <p class="testimonial-text">
                 
      <img src="images/home/icons/opening quote.png" class="opening-quote-position">
                 
      This is my testimonial.Read reviews also.VivoCarat delivered the necklace in 5 days.
                 
     </p> 
             
     <img src="images/home/icons/closing quote.png" class="closing-quote-position">
                
    </div>
   </div>
              
  </div>
             
 </div>
-->
           
</div> 
            
<!--
<div class="left carousel-control testimonial-arrow-structure" data-target="#testimonial" role="button" data-slide="prev">
 <img class="center-block testimonial-arrow-image" src="images/home/icons/testimonial - left arrow.png">
</div>

<div class="right carousel-control testimonial-arrow-structure" data-target="#testimonial" role="button" data-slide="next">
 <img class="center-block testimonial-arrow-image" src="images/home/icons/testimonial - right arrow.png">
</div>
-->
            
</div>
            
        
<!--


            <div class="row" style="padding-bottom:20px;">

                <div class="col-md-6 text-center" style="padding-right:0px;">
                    <div class="vivo-panel" style="height: 500px;border: 1px solid #d5d5d5;">
                        <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000" style="margin-top: 100px">
               
                            
                            <ol class="carousel-indicators">
                                <li data-target="#fade-quote-carousel" data-slide-to="0"></li>
                                <li data-target="#fade-quote-carousel" data-slide-to="1"></li>
                                <li data-target="#fade-quote-carousel" data-slide-to="2" class="active"></li>

                            </ol>
                           
                            
                            <div class="carousel-inner">
                                <div class="item testi">
                                    <div class="profile-circle"> <img src="images/testimonial/benson.jpg" class="img-responsive img-circle"></div>
                                    <blockquote>
                                        <p style="font-family: 'leela';font-size:13px;text-align:justify;">Beautiful collection of jewelry. The delivery packaging is the best amongst all ecomm sites. One happy customer.</p>
                                        <p style="font-family: 'leela';font-size:15px;color:#E62739 !important;text-transform:uppercase;">Benson</p>
                                    </blockquote>
                                </div>
                                <div class="item testi">
                                    <div class="profile-circle"> <img src="images/testimonial/ajinkya.jpg" class="img-responsive img-circle"></div>
                                    <blockquote>
                                        <p style="font-family: 'leela';font-size:13px;text-align:justify;">Had to get an anniversary gift for my wife. VivoCarat delivered the necklace in 5 days. Earlier than what was promised. Strongly recommend it.</p>
                                        <p style="font-family: 'leela';font-size:15px;color: #E62739 !important;text-transform:uppercase;">Ajinkya</p>
                                    </blockquote>
                                </div>
                                <div class="active item testi">
                                    <div class="profile-circle">
                                        <img src="images/testimonial/sarika.jpg" class="img-responsive img-circle"></div>
                                    <blockquote>
                                        <p style="font-family: 'leela';font-size:13px;text-align:justify;">VivoCarat sells the best jewelry available online. Smooth ordering and express delivery. Loved it.

                                        </p>
                                        <p style="font-family: 'leela';font-size:15px;color: #E62739 !important;text-transform:uppercase;">Sarika</p>
                                    </blockquote>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
-->

<div class="row padding-bottom-20px">
 <div class="col-lg-4 vivocarat-heading">
      OUR COMMITMENT
 </div>

 <div class="col-lg-8 categoryGrad no-padding-left-right margin-top-68px">

 </div>
</div>

  <!--4 ICONS wih link to about page-->

<div class="row normal-text our-commitment-structure">
 <div class="col-lg-3 text-center no-padding-left-right" data-ng-init="abouthover1='images/home/our commitment/trusted jewellers.png'" data-ng-mouseout="abouthover1='images/home/our commitment/trusted jewellers.png'" data-ng-mouseover="abouthover1='images/home/our commitment/trusted jewellers hover.png'">
  <a href="p-about.html">
   <img ng-src="{{abouthover1}}">
  </a>
  <p class="our-commitment-text">Trusted Jewellers</p>
 </div>
                
 <div class="col-lg-3 text-center no-padding-left-right" data-ng-init="abouthover2='images/home/our commitment/certified & hallmarked.png'" data-ng-mouseout="abouthover2='images/home/our commitment/certified & hallmarked.png'" data-ng-mouseover="abouthover2='images/home/our commitment/certified & hallmarked hover.png'">
  <a href="p-about.html">
   <img ng-src="{{abouthover2}}">
  </a>
  <p class="our-commitment-text">Certified &amp; Hallmarked</p>
 </div>
                
 <div class="col-lg-3 text-center no-padding-left-right" data-ng-init="abouthover3='images/home/our commitment/free shipping.png'" data-ng-mouseout="abouthover3='images/home/our commitment/free shipping.png'" data-ng-mouseover="abouthover3='images/home/our commitment/free shipping hover.png'">
  <a href="p-about.html">
   <img ng-src="{{abouthover3}}">
  </a>
  <p class="our-commitment-text">Free Shipping</p>
 </div>
                
 <div class="col-lg-3 text-center no-padding-left-right" data-ng-init="abouthover4='images/home/our commitment/easy return policy.png'" data-ng-mouseout="abouthover4='images/home/our commitment/easy return policy.png'" data-ng-mouseover="abouthover4='images/home/our commitment/easy return policy hover.png'">
  <a href="p-about.html">
   <img ng-src="{{abouthover4}}">
  </a>
  <p class="our-commitment-text">Easy Return Policy</p>
 </div>
                
</div>
<!--END of 4 ICONS wih link to about page-->


<div class="row padding-bottom-20px">
 <div class="col-lg-2 vivocarat-heading">
      MEDIA
 </div>

 <div class="col-lg-10 categoryGrad no-padding-left-right margin-top-68px">

 </div>
</div>
            
</div>
<!--End of container-->
        
<div class="row media-background-colour">
<div class="container">
    
<ul class="media-ul">
 <li class="media-li">         
  <a href="http://bwdisrupt.businessworld.in/article/VivoCarat-com-an-Online-Jewellery-Marketplace-Secures-50K-in-Seed-Funding/20-12-2016-110077/">
   <img class="media-logo-padding" src="images/home/media/business-standard-logo.png">
  </a>
 </li>
             
 <li class="media-li">  
  <a href="http://www.dealstreetasia.com/stories/india-dealbook-vivocarat-tiyo-events-high-raise-funding-60935/">
   <img class="media-logo-padding" src="images/home/media/deal-street-asia-logo.png">
  </a>
 </li>
             
 <li class="media-li">  
  <a href="http://techcircle.vccircle.com/2016/12/19/online-jewellery-marketplace-vivocarat-raises-seed-funding/">
   <img class="media-logo-padding" src="images/home/media/vcccircle logo.png">
  </a>
 </li>
             
 <li class="media-li">
  <a href="http://techstory.in/jewellery-vivocarat-funding-1912/">
   <img class="media-logo-padding" src="images/home/media/techstory logo.png">
  </a>
 </li>
             
 <li class="media-li">
  <a href="http://economictimes.indiatimes.com/small-biz/money/vivocarat-com-raises-50000-in-seed-funding/articleshow/56061909.cms">
   <img class="media-logo-padding" src="images/home/media/ettech-logo.png">
  </a>
 </li>
             
 <li class="media-li">  
  <a href="http://www.moneycontrol.com/news/sme/online-jewellery-store-vivocaratcom-raises-3650kseed-funding_8135561.html">
   <img class="media-logo-padding" src="images/home/media/money-control-logo.png">
  </a>
 </li>
             
 <li class="media-li">
  <a href="https://www.techinasia.com/4-rising-startups-in-india-dec-19-2016">
   <img class="media-logo-padding" src="images/home/media/techinasia-logo.png">
  </a>
 </li>
</ul>
             
</div>
<!--End of container div-->

</div>
        
<div class="container">
    
<div class="row padding-bottom-20px">
 <div class="col-lg-3 vivocarat-heading">
      SOCIAL MEDIA
 </div>

 <div class="col-lg-9 categoryGrad no-padding-left-right margin-top-68px">

 </div>
</div>
            
<div class="row padding-bottom-30px">
             
 <div class="col-md-2 text-center">
  <a href="https://twitter.com/vivocarat" title="Twitter">
   <img src="images/home/social media/twitter.png">  
  </a>   
 </div>
             
 <div class="col-md-2 text-center">
  <a href="https://www.facebook.com/VivoCarat/" title="Facebook">
   <img src="images/home/social media/fb.png">  
  </a>   
 </div>
             
 <div class="col-md-2 text-center">
  <a href="https://www.pinterest.com/vivocarat/" title="Pinterest">
   <img src="images/home/social media/pin.png">  
  </a>   
 </div>
             
 <div class="col-md-2 text-center">
  <a href="https://www.instagram.com/vivocarat/" title="Instagram">
   <img src="images/home/social media/insta.png">  
  </a>   
 </div>
             
 <div class="col-md-2 text-center">
  <a href="https://plus.google.com/vivocarat" title="Google +">
   <img src="images/home/social media/google+.png">  
  </a>   
 </div>
             
 <div class="col-md-2 text-center">
  <a href="https://www.linkedin.com/company/vivocarat" title="Linkedin">
   <img src="images/home/social media/linkedin.png">  
  </a>   
 </div>
    
</div>
    
</div>
<!--End of container div-->
        
        
        

</div>
</div>
</div>

</div>
<!--The above closing div is for controller-->

<vivo-footer></vivo-footer>
<script src="js/fwslider.js"></script>

<!--Script for making the image scroller clickable for navigating to Jeweller's page-->
<script>
 $(document).ready(function () {

  $('#incocu').click(function () {
    window.location.href = "p-store.html?store=Incocu Jewellers"
  });

  $('#shankaram').click(function () {
    window.location.href = "p-store.html?store=Shankaram Jewellers"
  });

  $('#lagu').click(function () {
    window.location.href = "p-store.html?store=Lagu Bandhu"
  });

  $('#arkina').click(function () {
    window.location.href = "p-store.html?store=Arkina-Diamonds"
  });

  $('#ornomart').click(function () {
    window.location.href = "p-store.html?store=OrnoMart"
  });

  $('#kundan').click(function () {
    window.location.href = "p-store.html?store=Kundan Jewellers"
  });

  $('#mayura').click(function () {
    window.location.href = "p-store.html?store=Mayura Jewellers"
  });

  $('#megha').click(function () {
    window.location.href = "p-store.html?store=Megha Jewellers"
  });
     
  $('#regaalia').click(function () {
    window.location.href = "p-store.html?store=Regaalia Jewels"
  });
     
  $('#glitter').click(function () {
    window.location.href = "p-store.html?store=Glitter Jewels"
  });
     
//  $('#mani').click(function () {
//    window.location.href = "p-store.html?store=Mani Jewels"
//  });
     
  $('#pp').click(function () {
    window.location.href = "p-store.html?store=PP-Gold"
  });
     
  $('#karatcraft').click(function () {
    window.location.href = "p-store.html?store=KaratCraft"
  });
     
  $('#zkd').click(function () {
    window.location.href = "p-store.html?store=ZKD-Jewels"
  });
     
  $('#iskiuski').click(function () {
    window.location.href = "p-store.html?store=Iski-Uski"
  });
     
  $('#myrah').click(function () {
    window.location.href = "p-store.html?store=Myrah-Silver-Works"
  });
     
  $('#charu').click(function () {
    window.location.href = "p-store.html?store=Charu-Jewels"
  });

 });
</script>

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-67690535-1', 'auto');
 ga('send', 'pageview');

</script>

</body>

</html>